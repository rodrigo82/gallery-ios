//
//  ViewController.swift
//  gallery-ios
//
//  Created by Rodrigo Silva on 04/10/2018.
//  Copyright © 2018 Rodrigo Silva. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {

    @IBOutlet weak var usernameInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    @IBAction func onLoginClick(_ sender: UIButton) {
        let username = usernameInput.text!
        let password = passwordInput.text!
        
        Auth.auth().signIn(withEmail: username, password: password){
            (auth, error) in
            if auth != nil {
                self.performSegue(withIdentifier: "toList", sender: self)
            }
        }
    }
}

